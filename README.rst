**W**\ ho **i**\ s **t**\ he **B**\ oss
=======================================

.. image:: https://gitlab.ub.uni-bielefeld.de/tl/who-is-the-boss/badges/master/pipeline.svg
        :target: https://gitlab.ub.uni-bielefeld.de/tl/who-is-the-boss
        :alt: Pipeline status badge


.. image:: https://gitlab.ub.uni-bielefeld.de/tl/who-is-the-boss/badges/master/coverage.svg
        :target: https://gitlab.ub.uni-bielefeld.de/tl/who-is-the-boss
        :alt: Coverage status badge



Abschlussprojekt 2019 - Netzwerk-Programmierung

http://bibiserv.cebitec.uni-bielefeld.de/resources/lehre/netprog19/Projekt_2019.pdf

Written in Python 3.7+ based on the `aiohttp
framework <http://docs.aiohttp.org/en/stable/>`_.

Description
-----------

Running on multiple peers, witb determines one and only one controller whereas the other
peers are used as workers. Controller and worker tasks are only performed as long as the
peer is part of the majority, preventing a split-brain scenario.

Commands to run are specified via the config file.

Demo
----
You can find a ``docker-compose.yml`` file inside the ``demo`` folder. It will launch 5
instances whose processes are managed by ``supervisord``. Launch them via
``docker-compose up``. Once only one controller process is running start stopping and
restarting peers, for example the current controller via ``docker-compose stop witb5``
and see how the remaining peers adapt to the new change.

Development
-----------

1. Get `Poetry <https://poetry.eustace.io/>`_ (or install it inside a venv to continue).
2. Install all dev requirements via ``poetry install``.
3. Setup all pre-commit hooks via ``poetry run pre-commit install``.

For direct feedback on small modification uncomment the corresponding volume inside
``demo/docker-compose.yml`` and simply restart the containers after a modification. Once
you add new packages via ``poetry`` you will have to rebuild the image.

Pytest is used to enforce the following code aspects:
  - functionality via tests and minimum of required coverage
  - codestyle via black and flake8

Documentation is available inside ``docs`` and can be build via ``make docs``.

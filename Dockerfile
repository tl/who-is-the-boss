FROM python:3-alpine

ADD src /code/src
ADD pyproject.toml poetry.lock /code/
WORKDIR /code
RUN apk update && apk add --no-cache gcc wget linux-headers musl-dev
RUN pip install --no-cache poetry supervisor
# install to system
RUN poetry config settings.virtualenvs.create false
# we do not need any development packages except aiohttp-devtools to
# automatically restart the app once we changed the bind-mounted source code
RUN poetry install --no-dev
CMD supervisord -c /etc/supervisord.conf

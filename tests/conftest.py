from asyncio import gather, sleep
from typing import List, Optional

from aiohttp.web import Application
from pytest import fixture

from witb.config import Config
from witb.main import create_app
from witb.model import Commander, Peer

DUMMY_COMMAND = "/bin/true"


def peers_from_ports(ports: List[int]) -> List[Peer]:
    return [Peer(host="localhost", port=port) for port in ports]


async def wait_for_startup_finished(apps: List[Application]) -> None:
    tasks = [app["startup_finished"] for app in apps]
    await gather(*tasks)


@fixture
def state_update():
    async def inner():
        await sleep(3)

    return inner


@fixture
async def witb_peers(
    aiohttp_client, aiohttp_server, aiohttp_unused_port, witb_create_app
):

    number_of_peers = 3
    ports = set()
    apps = []
    clients = []
    servers = []
    # to make sure we have random ports without duplicates
    while len(ports) != number_of_peers:
        ports = {aiohttp_unused_port() for _ in range(number_of_peers)}

    for port in ports:
        app = await witb_create_app(
            peers=peers_from_ports(list(ports - {port})), port=port
        )

        server = await aiohttp_server(app, port=port)
        client = await aiohttp_client(server)
        apps.append(app)
        clients.append(client)
        servers.append(server)
    await wait_for_startup_finished(apps)
    # await sleep(20)
    return apps, clients, servers


@fixture
async def witb_create_app(aiohttp_unused_port, loop):
    async def create_test_app(peers: List[Peer], port: int):
        return await create_app(
            config=Config(
                host="localhost",
                port=port,
                peer=peers if peers else [],
                controller=Commander(
                    start_command=DUMMY_COMMAND, stop_command=DUMMY_COMMAND
                ),
                worker=Commander(
                    start_command=DUMMY_COMMAND, stop_command=DUMMY_COMMAND
                ),
                sleep_time=1,
            )
        )

    return create_test_app

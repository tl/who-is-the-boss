from typing import List, Tuple

from aiohttp import ClientSession
from aiohttp.test_utils import TestClient, TestServer
from aiohttp.web import Application
from witb.config import Peer
from witb.lib import is_peer_reachable
from witb.main import PeerState, peers_reachable
from witb.views import PING_RESPONSE


def test_version():
    from witb import __version__

    assert __version__ == "0.1.0"


Peers = Tuple[List[Application], List[TestClient], List[TestServer]]


async def test_multipe_startup(witb_peers: Peers):
    """Extended version of :func:`test_single_startup`."""

    _, clients, _ = witb_peers
    resps = [await client.get("/ping") for client in clients]
    for resp in resps:
        text = await resp.text()
        assert (200, PING_RESPONSE) == (resp.status, text), "/ping endpoint failed"


class TestIsReachable:
    async def test_host_down(self,):
        async with ClientSession() as session:
            assert not await is_peer_reachable(Peer("localhost", 22), session)


async def test_peers_reachable(witb_peers: Peers):

    apps, _, _ = witb_peers
    async with ClientSession() as session:
        for app in apps:
            assert (
                len(await peers_reachable(app["config"].peers, session))
                == len(apps) - 1
            )


async def test_controller_task(witb_peers: Peers, aiohttp_server, state_update):
    apps, _, servers = witb_peers
    controller_list = [
        app for app in apps if app["config"].peer_state == PeerState.CONTROLLER
    ]
    assert len(controller_list) == 1
    controller_app = controller_list[0]
    # stop current controller
    await servers[apps.index(controller_app)].close()
    await state_update()
    controller_list = [
        app
        for app in apps
        if app["config"].peer_state == PeerState.CONTROLLER
        and app["config"].peers != controller_app["config"].peers
    ]
    assert len(controller_list) == 1
    await aiohttp_server(controller_app)
    await state_update()
    assert controller_app["config"].peer_state == PeerState.CONTROLLER


async def test_minority_detection(witb_peers: Peers, state_update):
    apps, _, servers = witb_peers
    number_of_peers = len(servers)
    while number_of_peers / 2 < len(servers):
        await servers.pop().close()
        apps.pop()
    await state_update()
    assert [app["config"].peer_state for app in apps] == [
        PeerState.NOT_SET_MINORITY
    ] * len(apps)

"""
Test constraints of configuration file
"""
import pytest
from pydantic import ValidationError

from witb.config import Config, Peer


def test_even_number_of_peers():
    """
    Even number of peers given odd number of controller, necessary to determines
    majority and minority.
    """

    with pytest.raises(ValidationError):

        Config(peer=Peer(host="127.0.0.1"))


def test_less_than():
    peer1 = Peer(host="127.0.0.1", port=5000)
    peer2 = Peer(host="127.0.0.2", port=4000)

    # The result does not matter, just make sure that they are comparable
    peer1 < peer2
    assert True

from pathlib import Path

import pytest

from witb.errors import CommandExitError, CommandNotFoundError
from witb.model import Commander


async def test_start_command(tmp_path: Path, loop):
    tst_file = tmp_path / "file.txt"
    tst_script = Path(__file__).parent / "bin" / "write_file.sh"
    commander = Commander(
        start_command=f"bash {tst_script.absolute()} test_start {tst_file.absolute()}",
        stop_command="",
    )
    await commander.start()
    assert tst_file.read_text() == "test_start\n"


async def test_command_not_found_error(loop):
    commander = Commander(start_command="nonexisting command", stop_command="")
    with pytest.raises(CommandNotFoundError):
        await commander.start()


async def test_command_run_error(loop):
    commander = Commander(start_command="/bin/false", stop_command="")
    with pytest.raises(CommandExitError) as e:
        await commander.start()
        assert e.returncode == 1

from pathlib import Path

import pytest

from witb import __version__
from witb.cli import main


def test_version(capsys):
    command = ["--version"]
    with pytest.raises(SystemExit):
        main(command)
    captured = capsys.readouterr()
    assert captured.out == f"{__version__}\n"


def test_no_config():
    returncode = main(["--debug"])
    assert returncode == 1


def test_invalid_toml(tmpdir):
    config_file = Path(tmpdir) / "config"
    config_file.write_text("foobar\n")
    command = ["--config", str(config_file)]
    returncode = main(command)

    assert returncode == 1


def test_invalid_config(tmpdir):
    config_file = Path(tmpdir) / "config"
    config_file.write_text("host = 1\n")
    command = ["--config", str(config_file)]
    returncode = main(command)

    assert returncode == 1


def test_xdg_config_home(tmpdir, monkeypatch, capsys):
    config_file = Path(tmpdir) / "witb.toml"
    config_file.touch()
    monkeypatch.setenv("XDG_CONFIG_HOME", str(tmpdir))
    print(str(tmpdir))

    with pytest.raises(SystemExit):
        main(["--help"])
    captured = capsys.readouterr().out
    # argparse help output is broken at 72 chars and indented
    concan_output = "".join([line.strip() for line in captured.splitlines()])
    assert f"{config_file}" in concan_output

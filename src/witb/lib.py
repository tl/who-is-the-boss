from __future__ import annotations

from asyncio import TimeoutError, gather
from logging import getLogger
from typing import Iterable, List, Tuple, cast

from aiohttp import ClientConnectorError, ClientSession

from .model import Peer
from .views import PING_ENDPOINT, PING_RESPONSE

_logger = getLogger(__name__)


async def peers_reachable(peers: Iterable[Peer], session: ClientSession) -> List[Peer]:
    """Count how many peers can be reached.

    Uses :func:`is_peer_reachable` asynchronously.

    :param Peers: Peers to contact
    :return: Number of reachable peers
    """
    tasks = [is_peer_reachable(peer, session) for peer in peers]
    up_results = cast(Tuple[bool], await gather(*tasks))
    return [peer for index, peer in enumerate(peers) if up_results[index]]


async def is_peer_reachable(
    peer: Peer, session: ClientSession, timeout: int = 5, retry: int = 3
) -> bool:
    """Try to reach the other peer by connecting to :data:`~witb.views.PING_ENDPOINT`.

    :param timeout: Timeout to use to initiate the connection.
    :param retry: Amount of retries until an attempt in considered unsuccessful.
    :return: Whether the other peer could be reached.
    """
    url = f"http://{peer.host}:{peer.port}{PING_ENDPOINT}"

    async def reach_peer() -> bool:
        async with session.get(url, timeout=timeout) as resp:
            return await resp.text() == PING_RESPONSE

    counter = 0
    while counter < retry:
        try:
            return await reach_peer()
        except (ClientConnectorError, TimeoutError) as exc:
            # peer not reachable
            counter += 1
            _logger.debug("Could not connect to peer %s: %s", peer, exc)
    _logger.warning("%s not reachable", peer)
    return False

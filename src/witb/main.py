"""Contains the main logic of the whole application."""
from asyncio import Queue, get_running_loop, sleep
from logging import getLogger
from typing import Any, Dict

from aiohttp import ClientSession, web
from aiojobs import Scheduler, create_scheduler

from .config import Config
from .errors import CommandExitError, CommandNotFoundError
from .lib import peers_reachable
from .model import PeerState
from .views import PING_ENDPOINT, ping

_logger = getLogger(__name__)


def task_exception_handler(scheduler: Scheduler, context: Dict[str, Any]) -> None:
    _logger.error(context.get("message"), exc_info=context.get("exception"))
    # In case of PYTHONASYNCIODEBUG=1
    if "source_traceback" in context:
        _logger.error("AsyncIO Traceback: %s", context["source_traceback"])


async def create_app(config: Config) -> web.Application:
    """
    Create an application and configure it.

    :param config: Configuration settings to run under.
    :return: Configured application instance.
    """

    app = web.Application()
    # startup is considered finished once all peers have been contacted
    # mainly used by testing code
    app.update(
        startup_finished=get_running_loop().create_future(),
        config=config,
        client_session=ClientSession(),
        state_queue=Queue(),
    )

    app.add_routes([web.get(PING_ENDPOINT, ping, name="ping")])

    app.on_startup.append(start_background_tasks)
    app.on_cleanup.append(stop_background_tasks)

    return app


async def start_background_tasks(app: web.Application) -> None:
    """Responsible for starting all background processes."""
    scheduler = await create_scheduler(exception_handler=task_exception_handler)
    await scheduler.spawn(peer_task(app))
    await scheduler.spawn(state_task(app))

    app["scheduler"] = scheduler


async def stop_background_tasks(app: web.Application) -> None:
    """Responsible for shutting down all background processes."""
    await app["client_session"].close()
    await app["scheduler"].close()


async def state_task(app: web.Application) -> None:
    """
    Responsible for running the commands associated with our state.

    Whenever a state is send to us from :func:`peer_task` first thing we check is
    whether it differs from our current one. If not nothing is done. If is does the
    following cases can happen:

    1. We are currently either WORKER or CONTROLLER:

       - Run the corresponding ``stop_command`` (see :ref:`Configuration`)
    2. Our new state is either WORKER or CONTROLLER:

       - Run the corresponding ``start_command`` (see :ref:`Configuration`)

    If we are now part of the minority no start command is run.

    :param app: Application instance holding all necessary information and our aiohttp
        session.
    """
    state_queue: Queue[PeerState] = app["state_queue"]
    config: Config = app["config"]
    while True:
        new_state: PeerState = await state_queue.get()
        if config.peer_state is new_state:
            # if state does not change we have nothing to do
            continue
        try:
            if config.peer_state is PeerState.CONTROLLER:
                await config.controller.stop()
            elif config.peer_state is PeerState.WORKER:
                await config.worker.stop()
        except (CommandExitError, CommandNotFoundError):
            _logger.exception("Stopping %s failed", config.peer_state)
        try:
            if new_state is PeerState.CONTROLLER:
                _logger.info("New State: Controller")
                await config.controller.start()
            elif new_state is PeerState.WORKER:
                _logger.info("New State: Worker")
                await config.worker.start()
        except (CommandExitError, CommandNotFoundError):
            _logger.exception("Starting %s failed", config.peer_state)
        if new_state is PeerState.NOT_SET_MINORITY:
            _logger.warning("New State: Minority -> Stopping all services.")

        # notify potential external observers (such as our testing framework)
        # that we now have definitive state
        if config.peer_state is PeerState.NOT_SET_YET:
            app["startup_finished"].set_result(True)
        config.peer_state = new_state
        state_queue.task_done()


async def peer_task(app: web.Application) -> None:
    """
    Responsible for connecting to the other peers and determining our new state.

    Tries to connect to other peers via :func:`~witb.lib.peers_reachable`. If this peer
    is part of the majority, meaning at least half of the other peers are reachable, we
    check whether we should be the controller among the reachable peers. If so
    :attr:`~witb.model.PeerState.CONTROLLER` is send to :func:`state_task` otherwise it
    is :attr:`~witb.model.PeerState.WORKER`.

    :param app: Application instance holding all necessary information and our aiohttp
        session.
    """
    config: Config = app["config"]
    session: ClientSession = app["client_session"]
    state_queue: Queue[PeerState] = app["state_queue"]

    while True:
        peers_up = await peers_reachable(config.peers, session)
        number_of_peers_up = len(peers_up)
        _logger.debug("%d/%d other peers are up", len(peers_up), len(config.peers))
        majority = len(config.peers) / 2 <= number_of_peers_up
        # Let's see whether we should be Controller
        if majority:
            if max(peers_up) < config.this_peer:
                await state_queue.put(PeerState.CONTROLLER)

            else:
                await state_queue.put(PeerState.WORKER)
        else:
            await state_queue.put(PeerState.NOT_SET_MINORITY)
        await sleep(config.sleep_time)

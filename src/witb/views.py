from aiohttp import web

PING_RESPONSE = "pong"
PING_ENDPOINT = "/ping"


async def ping(_: web.Request) -> web.Response:
    """
    Check whether application is up and running.

    :return: Response with the text body defined in :data:`PING_RESPONSE`.
    """
    return web.Response(text=PING_RESPONSE)

from asyncio.subprocess import PIPE, create_subprocess_exec
from enum import Enum, auto
from logging import getLogger
from typing import Any

from pydantic.dataclasses import dataclass

from .errors import CommandExitError, CommandNotFoundError

DEFAULT_PORT = 8000

_logger = getLogger(__name__)


class PeerState(Enum):
    """Encodes all possible states of a peer."""

    CONTROLLER = auto()
    WORKER = auto()
    NOT_SET_YET = auto()
    """Initial state."""
    NOT_SET_MINORITY = auto()
    """Set if this peer belongs to the minority."""


@dataclass(frozen=True)
class Peer:
    """Instance of this application running on another server."""

    host: str
    port: int = DEFAULT_PORT

    def __lt__(self, other: Any) -> bool:
        if not isinstance(other, Peer):
            raise NotImplementedError
        return (self.host, self.port) < (other.host, other.port)


@dataclass(frozen=True)
class Commander:

    start_command: str
    stop_command: str

    async def start(self) -> None:
        await Commander._run_command(self.start_command)

    async def stop(self) -> None:
        await Commander._run_command(self.stop_command)

    @staticmethod
    async def _run_command(command: str) -> None:
        try:
            proc = await create_subprocess_exec(
                *command.split(), stdout=PIPE, stderr=PIPE
            )
        except FileNotFoundError as e:
            raise CommandNotFoundError(e.args)
        stdout, stderr = await proc.communicate()
        if proc.returncode != 0:
            raise CommandExitError(
                command=command,
                stdout=stdout.decode(),
                stderr=stderr.decode(),
                returncode=proc.returncode,
            )
        _logger.info("Run `%s`", command)

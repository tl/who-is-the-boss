"""Command line interface and default entrypoint"""
import logging
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser, FileType
from os import getenv
from pathlib import Path
from typing import List, Optional

from aiohttp.web import run_app
from pydantic import ValidationError
from toml import TomlDecodeError, load

from . import __author__, __description__, __license__, __version__
from .config import Config
from .main import create_app

CONFIG_FILE = "witb.toml"


def detect_config_file() -> Optional[Path]:
    """
    Detect existing config files.

    Will first look in $XDG_CONFIG_HOME and all $XDG_CONFIG_DIRS later.

    :return: Existing config file.
    """
    config_dirs = [
        Path(getenv("XDG_CONFIG_HOME") or Path("~/.config").expanduser()),
        Path("/etc"),
    ]
    for _dir in config_dirs:
        if (_dir / CONFIG_FILE).exists():
            return _dir / CONFIG_FILE
    return None


def main(argv: Optional[List[str]] = None) -> int:
    """
    Main function of the application. Responsible for parsing command line options and
    starting the application.

    :param argv: Command line arguments to use instead of parsing sys.argv.
    :return: Exit code
    """
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        description=__description__,
        epilog=f"{__license__} @ {__author__}",
    )

    parser.add_argument(
        "-c",
        "--config",
        type=FileType(),
        help="Config file to use. See documentation for more details.",
        default=detect_config_file(),
    )
    verbosity_args = parser.add_mutually_exclusive_group()
    verbosity_args.add_argument(
        "-d", "--debug", action="store_true", help="Show DEBUG messages"
    )
    verbosity_args.add_argument(
        "-s", "--silent", action="store_true", help="Only show ERROR messages"
    )
    parser.add_argument("-v", "--version", action="version", version=__version__)
    args = parser.parse_args(argv)

    logger = logging.getLogger("witb")
    if args.debug:
        logger.setLevel(logging.DEBUG)
    elif args.silent:
        logger.setLevel(logging.WARNING)
    else:
        logger.setLevel(logging.INFO)
    logging.debug("Running with args: %s", args)

    if not args.config:
        logging.error("Can not operate without config file. Exiting")
        return 1

    try:
        config = Config(**load(args.config))
    except TomlDecodeError as exc:
        logging.error("Invalid TOML [%s]. Exiting", exc)
        return 1
    except ValidationError as exc:
        logging.error("Invalid config file:\n%s", exc)
        return 1
    logging.debug("Running with config %s", config)

    run_app(create_app(config), host=config.host, port=config.port)
    return 0


if __name__ == "__main__":
    exit(main())

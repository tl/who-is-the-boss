# noqa: D400
"""
Running on multiple peers, witb determines one and only one controller whereas the other
peers are used as workers. Controller and worker tasks are only performed as long as the
peer is part of the majority, preventing a split-brain scenario.

Commands to run are specified via the config file.
"""

__version__ = "0.1.0"
__author__ = "Tilman Lüttje"
__description__ = __doc__
__license__ = "AGPL v3"

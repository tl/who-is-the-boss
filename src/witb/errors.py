"""Contains all errors thrown by this application."""


class WitbError(Exception):
    """Base exception of all errors raised by the package."""

    pass


class CommandNotFoundError(WitbError):
    """Raised if the specified command cannot be found."""

    pass


class CommandExitError(WitbError):
    """Raised if a controller/worker command exits with a non-zero returncode."""

    def __init__(self, returncode: int, stdout: str, stderr: str, command: str) -> None:
        self.returncode = returncode
        self.stdout = stdout
        self.stderr = stderr
        self.command = command

    def __str__(self) -> str:
        return (
            f"Command `{self.command}` exited with status {self.returncode}. "
            f"Captured stdout: `{self.stdout}`, stderr: `{self.stderr}`"
        )

"""Configuration. Uses `Pydantic <https://pydantic-docs.helpmanual.io/#settings>`_ to
enforce types of passed arguments."""
from __future__ import annotations

from typing import List

from pydantic import BaseModel, PositiveInt, validator

from .model import DEFAULT_PORT, Commander, Peer, PeerState


class Config(BaseModel):
    """Configuration settings of the application."""

    host: str
    """Hostname of this peer. Has to match the one used in the ``peer`` section."""
    port: int = DEFAULT_PORT
    """Port of the application."""

    # not named peers to match style of toml config file
    peer: List[Peer]
    """Other peers running ``witb``. Every one is configured inside a single
    ``[[peer]]`` section inside the config file."""

    controller: Commander
    """Holds ``start`` and ``stop_command`` for the controller process inside a
    :class:`~witb.model.Commander` instance."""
    worker: Commander
    """Holds ``start`` and ``stop_command`` for the worker process inside a
    :class:`~witb.model.Commander` instance."""

    sleep_time: PositiveInt = PositiveInt(10)
    """Interval between each check. Decreasing its value fastens quorum detection and
    increases the amount of requests sent over your network."""

    peer_state = PeerState.NOT_SET_YET
    """Current state of this peer, one of :class:`~witb.model.PeerState`. Initially
    :attr:`~witb.model.PeerState.NOT_SET_YET` until a first quorum is build."""

    @property
    def this_peer(self) -> Peer:
        """
        Provide data of this peer in a manner directly comparable with other peers.

        :return: This Peer.
        """
        return Peer(self.host, self.port)

    @validator("peer", whole=True)
    def _total_odd_number_of_peers(cls, value: List[Peer]) -> List[Peer]:
        """
        Enforce total number odd peers.

        Therefore number of additional peers be even to detect minorities.
        """
        if len(value) % 2 == 1:
            raise ValueError(__doc__)
        return value

    @property
    def peers(self) -> List[Peer]:
        """Alias to ease usage."""
        return self.peer

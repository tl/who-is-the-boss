.. Who is the boss? documentation master file, created by
   sphinx-quickstart on Sun Jul  7 14:56:17 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README.rst

Usage
-----

.. program-output:: witb --help

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   configuration
   functioning
   modules


How does it work?
-----------------

Every peer does continuously check whether every other peer is reachable. Than every
peer determines itself should be the controller or a worker among the reachable peers
**but only** if a majority of all configured peers could be reached.



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Functioning
===========

The tasks documented below are communicating via a :class:`asyncio.Queue`.

.. autofunction:: witb.main.peer_task
   :noindex:

.. autofunction:: witb.main.state_task
   :noindex:

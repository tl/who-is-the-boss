Configuration
=============

The configuration is done via a config file in `TOML
<https://github.com/toml-lang/toml>`_. An example can be found in
``dist/witb.toml.dist``.

At startup a file named ``witb.toml`` located either at ``$XDG_CONFIG_HOME`` or ``/etc``
is automatically loaded. This location can be changed via ``--config``.

.. literalinclude:: ../dist/witb.toml.dist
   :language: toml

.. autoclass:: witb.config.Config
   :members:
   :undoc-members:
   :noindex:
